//
//  FCLiquidLayout.h
//  LiquidOrientation
//
//  Created by aFrogleap on 11/20/12.
//  Copyright (c) 2012 farcoding. All rights reserved.
//


#import <UIKit/UIKit.h>


typedef enum
{
    FCLiquidUnitPercentage = 0,
    FCLiquidUnitPoints = 1
} FCLiquidUnit;



typedef struct FCLiquidUnits
{
    FCLiquidUnit unit1;
    FCLiquidUnit unit2;
    FCLiquidUnit unit3;
    FCLiquidUnit unit4;
} FCLiquidUnits;


typedef CGRect FCLiquidFrame;
typedef UIEdgeInsets FCLiquidMargin;
typedef UIEdgeInsets FCLiquidPadding;
typedef CGPoint FCLiquidOrigin;
typedef CGSize FCLiquidSize;

typedef struct FCLiquidContainerInfo
{
    FCLiquidMargin margin;
    FCLiquidSize size;
} FCLiquidContainerInfo;



FCLiquidFrame FCLiquidFrameMake(CGFloat x, CGFloat y, CGFloat width, CGFloat height);
FCLiquidMargin FCLiquidMarginMake(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right);
FCLiquidPadding FCLiquidPaddingMake(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right);
FCLiquidUnits FCLiquidUnitsMake(FCLiquidUnit unit1, FCLiquidUnit unit2, FCLiquidUnit unit3, FCLiquidUnit unit4);
FCLiquidUnits FCLiquidPercentageUnits();
FCLiquidUnits FCLiquidPointsUnits();
FCLiquidOrigin FCLiquidOriginMake(CGFloat x, CGFloat y);
FCLiquidSize FCLiquidSizeMake(CGFloat width, CGFloat height);



@protocol FCLiquidLayout <NSObject>

@required

@property (nonatomic) BOOL supportsLiquidLayout;

@property (nonatomic, copy) NSString *liquidLayoutID;

@property (nonatomic) FCLiquidFrame liquidFrame;
@property (nonatomic) FCLiquidUnits liquidFrameUnits;

@property (nonatomic) FCLiquidFrame liquidPortraitFrame;
@property (nonatomic) FCLiquidUnits liquidPortraitFrameUnits;

@property (nonatomic) FCLiquidFrame liquidLandscapeFrame;
@property (nonatomic) FCLiquidUnits liquidLandscapeFrameUnits;

@property (nonatomic) FCLiquidMargin liquidMargin;
@property (nonatomic) FCLiquidUnits liquidMarginUnits;

@property (nonatomic) FCLiquidMargin liquidPortraitMargin;
@property (nonatomic) FCLiquidUnits liquidPortraitMarginUnits;

@property (nonatomic) FCLiquidMargin liquidLandscapeMargin;
@property (nonatomic) FCLiquidUnits liquidLandscapeMarginUnits;

@property (nonatomic) FCLiquidPadding liquidPadding;
@property (nonatomic) FCLiquidUnits liquidPaddingUnits;

@property (nonatomic) FCLiquidPadding liquidPortraitPadding;
@property (nonatomic) FCLiquidUnits liquidPortraitPaddingUnits;

@property (nonatomic) FCLiquidPadding liquidLandscapePadding;
@property (nonatomic) FCLiquidUnits liquidLandscapePaddingUnits;


- (id<FCLiquidLayout>)childWithLiquidLayoutID:(NSString *)liquidLayoutID;

@end



@interface FCLiquidLayout : NSObject

+ (CGFloat)aspectRatio;

+ (FCLiquidContainerInfo)liquidInfoForContainer:(id<FCLiquidLayout>)container
                                   originalSize:(FCLiquidSize)originalSize
                                    orientation:(UIDeviceOrientation)orientation;

+ (FCLiquidFrame)liquidFrameForObject:(id<FCLiquidLayout>)liquidObject
                        containerInfo:(FCLiquidContainerInfo)containerInfo
                          orientation:(UIDeviceOrientation)orientation;

@end



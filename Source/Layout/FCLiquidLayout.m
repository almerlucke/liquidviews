//
//  FCLiquidFrame.m
//  LiquidOrientation
//
//  Created by aFrogleap on 11/20/12.
//  Copyright (c) 2012 farcoding. All rights reserved.
//


#import "FCLiquidLayout.h"



@interface FCLiquidLayout ()

@property (nonatomic) CGFloat aspectRatio;

@end


@implementation FCLiquidLayout

+ (FCLiquidLayout *)sharedInstance
{
    static dispatch_once_t _token;
    static FCLiquidLayout *_sharedInstance;
    
    dispatch_once(&_token, ^{
        _sharedInstance = [[FCLiquidLayout alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        CGRect bounds = [FCLiquidLayout screenBoundsFixedToPortraitOrientation];
        
        _aspectRatio = bounds.size.width / bounds.size.height;
    }
    
    return self;
}

+ (CGFloat)aspectRatio
{
    return [self sharedInstance].aspectRatio;
}

+ (CGRect)screenBoundsFixedToPortraitOrientation {
    UIScreen *screen = [UIScreen mainScreen];
    CGRect bounds = screen.bounds;
    
    if ([screen respondsToSelector:@selector(fixedCoordinateSpace)]) {
        bounds = [screen.coordinateSpace convertRect:bounds
                                   toCoordinateSpace:screen.fixedCoordinateSpace];
    }
    
    return bounds;
}

+ (FCLiquidContainerInfo)liquidInfoForContainer:(id<FCLiquidLayout>)container
                                   originalSize:(FCLiquidSize)originalSize
                                    orientation:(UIDeviceOrientation)orientation
{
    CGFloat containerWidth = originalSize.width;
    CGFloat containerHeight = originalSize.height;
    BOOL isLandscape = UIDeviceOrientationIsLandscape(orientation);
    
    FCLiquidMargin tmpLiquidMargin = isLandscape? container.liquidLandscapeMargin : container.liquidPortraitMargin;
    FCLiquidUnits tmpLiquidMarginUnits = isLandscape? container.liquidLandscapeMarginUnits : container.liquidPortraitMarginUnits;
    
    CGFloat marginTop = (tmpLiquidMarginUnits.unit1 == FCLiquidUnitPercentage)? containerHeight * tmpLiquidMargin.top : tmpLiquidMargin.top;
    CGFloat marginLeft = (tmpLiquidMarginUnits.unit2 == FCLiquidUnitPercentage)? containerWidth * tmpLiquidMargin.left : tmpLiquidMargin.left;
    CGFloat marginBottom = (tmpLiquidMarginUnits.unit3 == FCLiquidUnitPercentage)? containerHeight * tmpLiquidMargin.bottom : tmpLiquidMargin.bottom;
    CGFloat marginRight = (tmpLiquidMarginUnits.unit4 == FCLiquidUnitPercentage)? containerWidth * tmpLiquidMargin.right : tmpLiquidMargin.right;
    
    FCLiquidContainerInfo info;
    
    info.size = FCLiquidSizeMake(containerWidth - (marginLeft + marginRight),
                                 containerHeight - (marginTop + marginBottom));
    
    info.margin = FCLiquidMarginMake(marginTop, marginLeft, marginBottom, marginRight);
    
    return info;
}

+ (FCLiquidFrame)liquidFrameForObject:(id<FCLiquidLayout>)liquidObject
                        containerInfo:(FCLiquidContainerInfo)containerInfo
                          orientation:(UIDeviceOrientation)orientation
{
    BOOL isLandscape = UIDeviceOrientationIsLandscape(orientation);
    FCLiquidSize containerSize = containerInfo.size;
    FCLiquidMargin containerMargin = containerInfo.margin;
    FCLiquidPadding liquidPadding = isLandscape? liquidObject.liquidLandscapePadding : liquidObject.liquidPortraitPadding;
    FCLiquidUnits liquidPaddingUnits = isLandscape? liquidObject.liquidLandscapePaddingUnits : liquidObject.liquidPortraitPaddingUnits;
    FCLiquidOrigin liquidOrigin = isLandscape? liquidObject.liquidLandscapeFrame.origin : liquidObject.liquidPortraitFrame.origin;
    FCLiquidSize liquidSize = isLandscape? liquidObject.liquidLandscapeFrame.size : liquidObject.liquidPortraitFrame.size;
    FCLiquidUnits liquidFrameUnits = isLandscape? liquidObject.liquidLandscapeFrameUnits : liquidObject.liquidPortraitFrameUnits;
    
    CGFloat originX = (liquidFrameUnits.unit1 == FCLiquidUnitPercentage)? containerSize.width * liquidOrigin.x : liquidOrigin.x;
    CGFloat originY = (liquidFrameUnits.unit2 == FCLiquidUnitPercentage)? containerSize.height * liquidOrigin.y : liquidOrigin.y;
    CGFloat width = (liquidFrameUnits.unit3 == FCLiquidUnitPercentage)? containerSize.width * liquidSize.width : liquidSize.width;
    CGFloat height = (liquidFrameUnits.unit4 == FCLiquidUnitPercentage)? containerSize.height * liquidSize.height : liquidSize.height;
    
    CGFloat paddingTop = (liquidPaddingUnits.unit1 == FCLiquidUnitPercentage)? height * liquidPadding.top : liquidPadding.top;
    CGFloat paddingLeft = (liquidPaddingUnits.unit2 == FCLiquidUnitPercentage)? width * liquidPadding.left : liquidPadding.left;
    CGFloat paddingBottom = (liquidPaddingUnits.unit3 == FCLiquidUnitPercentage)? height * liquidPadding.bottom : liquidPadding.bottom;
    CGFloat paddingRight = (liquidPaddingUnits.unit4 == FCLiquidUnitPercentage)? width * liquidPadding.right : liquidPadding.right;
    
    return FCLiquidFrameMake(floorf(containerMargin.left + paddingLeft + originX),
                             floorf(containerMargin.top + paddingTop + originY),
                             floorf(width - (paddingLeft + paddingRight)),
                             floorf(height - (paddingTop + paddingBottom)));
}

@end





FCLiquidFrame FCLiquidFrameMake(CGFloat x, CGFloat y, CGFloat width, CGFloat height)
{
    return CGRectMake(x, y, width, height);
}

FCLiquidMargin FCLiquidMarginMake(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right)
{
    return UIEdgeInsetsMake(top, left, bottom, right);
}

FCLiquidPadding FCLiquidPaddingMake(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right)
{
    return UIEdgeInsetsMake(top, left, bottom, right);
}

FCLiquidUnits FCLiquidUnitsMake(FCLiquidUnit unit1, FCLiquidUnit unit2, FCLiquidUnit unit3, FCLiquidUnit unit4)
{
    FCLiquidUnits units = {unit1, unit2, unit3, unit4};
    
    return units;
}

FCLiquidOrigin FCLiquidOriginMake(CGFloat x, CGFloat y)
{
    return CGPointMake(x, y);
}

FCLiquidSize FCLiquidSizeMake(CGFloat width, CGFloat height)
{
    return CGSizeMake(width, height);
}

FCLiquidUnits FCLiquidPercentageUnits()
{
    return FCLiquidUnitsMake(FCLiquidUnitPercentage, FCLiquidUnitPercentage, FCLiquidUnitPercentage, FCLiquidUnitPercentage);
}

FCLiquidUnits FCLiquidPointsUnits()
{
    return FCLiquidUnitsMake(FCLiquidUnitPoints, FCLiquidUnitPoints, FCLiquidUnitPoints, FCLiquidUnitPoints);
}



//
//  UIView+FCLiquidFrame.m
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/4/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "UIView+FCLiquidLayout.h"
#import "NSObject+RuntimeHelpers.h"
#import "FCLiquidLayoutLoader.h"


@implementation UIView (FCLiquidLayout)

#pragma mark - Init

+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzle:@selector(layoutSubviews) for:@selector(liquidLayoutSubviews)];
    });
}

- (void)liquidLayoutSubviews
{
    // first call the original layoutSubviews
    [self liquidLayoutSubviews];
    
    if (self.supportsLiquidLayout) {
        UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
        FCLiquidContainerInfo containerInfo = [FCLiquidLayout liquidInfoForContainer:self
                                                                        originalSize:self.bounds.size
                                                                         orientation:orientation];
        
        for (UIView *subview in self.subviews) {
            if (subview.supportsLiquidLayout) {
                subview.frame = [FCLiquidLayout liquidFrameForObject:subview
                                                       containerInfo:containerInfo
                                                         orientation:orientation];
            }
        }
    }
}

- (instancetype)initWithLiquidFrame:(CGRect)liquidFrame
{
    if ((self = [self init])) {
        [self setLiquidFrame:liquidFrame];
        [self setSupportsLiquidLayout:YES];
    }
    
    return self;
}

- (void)loadLiquidLayout
{
    [FCLiquidLayoutLoader loadLayoutForObject:self];
}


#pragma mark - Liquid Layout Required Properties

- (BOOL)supportsLiquidLayout
{
    NSNumber *supportsLiquidLayout = [self assocForKey:@selector(supportsLiquidLayout)];
    
    return [supportsLiquidLayout boolValue];
}

- (void)setSupportsLiquidLayout:(BOOL)supportsLiquidLayout
{
    [self setAssoc:[NSNumber numberWithBool:supportsLiquidLayout] forKey:@selector(supportsLiquidLayout)];
}

- (NSString *)liquidLayoutID
{
    return [self assocForKey:@selector(liquidLayoutID)];
}

- (void)setLiquidLayoutID:(NSString *)liquidLayoutID
{
    [self setAssoc:liquidLayoutID forKey:@selector(liquidLayoutID) policy:OBJC_ASSOCIATION_COPY_NONATOMIC];
}

- (FCLiquidFrame)liquidFrame
{
    return self.liquidPortraitFrame;
}

- (void)setLiquidFrame:(FCLiquidFrame)liquidFrame
{
    self.liquidPortraitFrame = liquidFrame;
    self.liquidLandscapeFrame = liquidFrame;
    
    [self setNeedsLayout];
}

- (FCLiquidUnits)liquidFrameUnits
{
    return self.liquidPortraitFrameUnits;
}

- (void)setLiquidFrameUnits:(FCLiquidUnits)liquidUnits
{
    self.liquidPortraitFrameUnits = liquidUnits;
    self.liquidLandscapeFrameUnits = liquidUnits;
    
    [self setNeedsLayout];
}

- (FCLiquidFrame)liquidPortraitFrame
{
    NSValue *liquidPortraitFrame = [self assocForKey:@selector(liquidPortraitFrame)];
    
    return [liquidPortraitFrame CGRectValue];
}

- (void)setLiquidPortraitFrame:(FCLiquidFrame)liquidFrame
{
    [self setAssoc:[NSValue valueWithCGRect:liquidFrame]
            forKey:@selector(liquidPortraitFrame)];
    
    [self setNeedsLayout];
}

- (FCLiquidUnits)liquidPortraitFrameUnits
{
    NSValue *liquidPortraitFrameUnits = [self assocForKey:@selector(liquidPortraitFrameUnits)];
    FCLiquidUnits units = {0, 0, 0, 0};
    
    [liquidPortraitFrameUnits getValue:&units];
    
    return units;
}

- (void)setLiquidPortraitFrameUnits:(FCLiquidUnits)liquidUnits
{
    [self setAssoc:[NSValue value:&liquidUnits withObjCType:@encode(FCLiquidUnits)]
            forKey:@selector(liquidPortraitFrameUnits)];
    
    [self setNeedsLayout];
}

- (FCLiquidFrame)liquidLandscapeFrame
{
    NSValue *liquidLandscapeFrame = [self assocForKey:@selector(liquidLandscapeFrame)];
    
    return [liquidLandscapeFrame CGRectValue];
}

- (void)setLiquidLandscapeFrame:(FCLiquidFrame)liquidFrame
{
    [self setAssoc:[NSValue valueWithCGRect:liquidFrame]
            forKey:@selector(liquidLandscapeFrame)];
    
    [self setNeedsLayout];
}

- (FCLiquidUnits)liquidLandscapeFrameUnits
{
    NSValue *liquidLandscapeFrameUnits = [self assocForKey:@selector(liquidLandscapeFrameUnits)];
    FCLiquidUnits units = {0, 0, 0, 0};
    
    [liquidLandscapeFrameUnits getValue:&units];
    
    return units;
}

- (void)setLiquidLandscapeFrameUnits:(FCLiquidUnits)liquidUnits
{
    [self setAssoc:[NSValue value:&liquidUnits withObjCType:@encode(FCLiquidUnits)]
            forKey:@selector(liquidLandscapeFrameUnits)];
    
    [self setNeedsLayout];
}

- (FCLiquidMargin)liquidMargin
{
    return self.liquidPortraitMargin;
}

- (void)setLiquidMargin:(FCLiquidMargin)liquidMargin
{
    self.liquidPortraitMargin = liquidMargin;
    self.liquidLandscapeMargin = liquidMargin;
    
    [self setNeedsLayout];
}

- (FCLiquidUnits)liquidMarginUnits
{
    return self.liquidPortraitMarginUnits;
}

- (void)setLiquidMarginUnits:(FCLiquidUnits)liquidMarginUnits
{
    self.liquidPortraitMarginUnits = liquidMarginUnits;
    self.liquidLandscapeMarginUnits = liquidMarginUnits;
    
    [self setNeedsLayout];
}

- (FCLiquidMargin)liquidPortraitMargin
{
    NSValue *liquidPortraitMargin = [self assocForKey:@selector(liquidPortraitMargin)];
    
    return [liquidPortraitMargin UIEdgeInsetsValue];
}

- (void)setLiquidPortraitMargin:(FCLiquidMargin)liquidMargin
{
    [self setAssoc:[NSValue valueWithUIEdgeInsets:liquidMargin]
            forKey:@selector(liquidPortraitMargin)];
    
    [self setNeedsLayout];
}

- (FCLiquidUnits)liquidPortraitMarginUnits
{
    NSValue *liquidPortraitMarginUnits = [self assocForKey:@selector(liquidPortraitMarginUnits)];
    FCLiquidUnits units = {0, 0, 0, 0};
    
    [liquidPortraitMarginUnits getValue:&units];
    
    return units;
}

- (void)setLiquidPortraitMarginUnits:(FCLiquidUnits)liquidUnits
{
    [self setAssoc:[NSValue value:&liquidUnits withObjCType:@encode(FCLiquidUnits)]
            forKey:@selector(liquidPortraitMarginUnits)];
    
    [self setNeedsLayout];
}

- (FCLiquidMargin)liquidLandscapeMargin
{
    NSValue *liquidLandscapeMargin = [self assocForKey:@selector(liquidLandscapeMargin)];
    
    return [liquidLandscapeMargin UIEdgeInsetsValue];
}

- (void)setLiquidLandscapeMargin:(FCLiquidMargin)liquidMargin
{
    [self setAssoc:[NSValue valueWithUIEdgeInsets:liquidMargin]
            forKey:@selector(liquidLandscapeMargin)];
    
    [self setNeedsLayout];
}

- (FCLiquidUnits)liquidLandscapeMarginUnits
{
    NSValue *liquidLandscapeMarginUnits = [self assocForKey:@selector(liquidLandscapeMarginUnits)];
    FCLiquidUnits units = {0, 0, 0, 0};
    
    [liquidLandscapeMarginUnits getValue:&units];
    
    return units;
}

- (void)setLiquidLandscapeMarginUnits:(FCLiquidUnits)liquidUnits
{
    [self setAssoc:[NSValue value:&liquidUnits withObjCType:@encode(FCLiquidUnits)]
            forKey:@selector(liquidLandscapeMarginUnits)];
    
    [self setNeedsLayout];
}

- (FCLiquidPadding)liquidPadding
{
    return self.liquidPortraitPadding;
}

- (void)setLiquidPadding:(FCLiquidPadding)liquidPadding
{
    self.liquidPortraitPadding = liquidPadding;
    self.liquidLandscapePadding = liquidPadding;
    
    [self setNeedsLayout];
}

- (FCLiquidUnits)liquidPaddingUnits
{
    return self.liquidPortraitPaddingUnits;
}

- (void)setLiquidPaddingUnits:(FCLiquidUnits)liquidPaddingUnits
{
    self.liquidPortraitPaddingUnits = liquidPaddingUnits;
    self.liquidLandscapePaddingUnits = liquidPaddingUnits;
    
    [self setNeedsLayout];
}

- (FCLiquidPadding)liquidPortraitPadding
{
    NSValue *liquidPortraitPadding = [self assocForKey:@selector(liquidPortraitPadding)];
    
    return [liquidPortraitPadding UIEdgeInsetsValue];
}

- (void)setLiquidPortraitPadding:(FCLiquidPadding)liquidPadding
{
    [self setAssoc:[NSValue valueWithUIEdgeInsets:liquidPadding]
            forKey:@selector(liquidPortraitPadding)];
    
    [self setNeedsLayout];
}

- (FCLiquidUnits)liquidPortraitPaddingUnits
{
    NSValue *liquidPortraitPaddingUnits = [self assocForKey:@selector(liquidPortraitPaddingUnits)];
    FCLiquidUnits units = {0, 0, 0, 0};
    
    [liquidPortraitPaddingUnits getValue:&units];
    
    return units;
}

- (void)setLiquidPortraitPaddingUnits:(FCLiquidUnits)liquidUnits
{
    [self setAssoc:[NSValue value:&liquidUnits withObjCType:@encode(FCLiquidUnits)]
            forKey:@selector(liquidPortraitPaddingUnits)];
    
    [self setNeedsLayout];
}

- (FCLiquidPadding)liquidLandscapePadding
{
    NSValue *liquidLandscapePadding = [self assocForKey:@selector(liquidLandscapePadding)];
    
    return [liquidLandscapePadding UIEdgeInsetsValue];
}

- (void)setLiquidLandscapePadding:(FCLiquidPadding)liquidPadding
{
    [self setAssoc:[NSValue valueWithUIEdgeInsets:liquidPadding]
            forKey:@selector(liquidLandscapePadding)];
    
    [self setNeedsLayout];
}

- (FCLiquidUnits)liquidLandscapePaddingUnits
{
    NSValue *liquidLandscapePaddingUnits = [self assocForKey:@selector(liquidLandscapePaddingUnits)];
    FCLiquidUnits units = {0, 0, 0, 0};
    
    [liquidLandscapePaddingUnits getValue:&units];
    
    return units;
}

- (void)setLiquidLandscapePaddingUnits:(FCLiquidUnits)liquidUnits
{
    [self setAssoc:[NSValue value:&liquidUnits withObjCType:@encode(FCLiquidUnits)]
            forKey:@selector(liquidLandscapePaddingUnits)];
    
    [self setNeedsLayout];
}


#pragma mark - Liquid Layout Required Methods

- (id<FCLiquidLayout>)childWithLiquidLayoutID:(NSString *)liquidLayoutID
{
    for (UIView *subview in self.subviews) {
        if ([subview.liquidLayoutID isEqualToString:liquidLayoutID]) {
            return subview;
        }
    }
    
    return nil;
}

@end

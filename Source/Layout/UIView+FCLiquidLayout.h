//
//  UIView+FCLiquidFrame.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/4/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayout.h"
#import <UIKit/UIKit.h>


@interface UIView (FCLiquidLayout) <FCLiquidLayout>

@property (nonatomic) IBInspectable BOOL supportsLiquidLayout;

@property (nonatomic, copy) IBInspectable NSString *liquidLayoutID;

- (instancetype)initWithLiquidFrame:(CGRect)liquidFrame;

- (void)loadLiquidLayout;

@end

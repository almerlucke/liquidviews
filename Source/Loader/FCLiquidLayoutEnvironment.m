//
//  FCLiquidLayoutEnvironment.m
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/15/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutEnvironment.h"
#import "FCLiquidLayout.h"
#import "FCLiquidLayoutInterpreter.h"


@interface FCLiquidLayoutEnvironment ()

@property (nonatomic, strong) NSMutableArray *scopes;

@end


@implementation FCLiquidLayoutEnvironment

- (instancetype)init
{
    self = [super init];
    if (self) {
        _scopes = [NSMutableArray array];
        [_scopes addObject:[@{@"aspectRatio" : @([FCLiquidLayout aspectRatio])} mutableCopy]];
    }
    
    return self;
}

- (void)popScope
{
    if (self.scopes.count > 0) {
        [self.scopes removeObjectAtIndex:0];
    }
}

- (NSMutableDictionary *)pushScope
{
    NSMutableDictionary *scope = [NSMutableDictionary dictionary];
    
    [self.scopes insertObject:scope atIndex:0];
    
    return scope;
}

- (NSNumber *)bindingForSymbol:(NSString *)symbol
{
    NSNumber *binding = nil;
    
    for (NSDictionary *scope in self.scopes) {
        binding = scope[symbol];
        if (binding) {
            break;
        }
    }
    
    return binding;
}

- (void)addBinding:(id)binding forSymbol:(NSString *)symbol toScope:(NSMutableDictionary *)scope
{
    if ([symbol isKindOfClass:[NSString class]]) {
        if ([binding isKindOfClass:[NSNumber class]]) {
            scope[symbol] = binding;
        } else if ([binding isKindOfClass:[NSString class]]) {
            @try {
                NSNumber *interpretedBinding = [FCLiquidLayoutInterpreter interpretString:binding inEnvironment:self];
                if (interpretedBinding) {
                    scope[symbol] = interpretedBinding;
                }
            }
            @catch (NSException *exception) {
                NSLog(@"set var %@ from loader failed: %@", symbol, binding);
            }
        }
    }
}

- (void)pushScopeWithBindings:(NSArray *)bindings
{
    NSMutableDictionary *scope = [self pushScope];
    
    if ([bindings isKindOfClass:[NSArray class]]) {
        for (NSDictionary *binding in bindings) {
            if ([binding isKindOfClass:[NSDictionary class]]) {
                for (id symbol in binding.allKeys) {
                    [self addBinding:binding[symbol] forSymbol:symbol toScope:scope];
                }
            }
        }
    }
}

@end

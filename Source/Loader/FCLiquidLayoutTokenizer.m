//
//  FCLiquidLayoutParser.m
//  LiquidOrientation
//
//  Created by Almer Lucke on 14/12/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutTokenizer.h"
#import "FCLiquidLayout.h"





@interface FCLiquidLayoutTokenizerToken ()

@property (nonatomic) FCLiquidLayoutTokenizerTokenType tokenType;

@property (nonatomic) FCLiquidLayoutTokenizerOperatorType operatorType;

@property (nonatomic, strong) NSNumber *numberValue;

@property (nonatomic, copy) NSString *symbolValue;

@end



@implementation FCLiquidLayoutTokenizerToken

+ (FCLiquidLayoutTokenizerToken *)tokenWithType:(FCLiquidLayoutTokenizerTokenType)type
{
    FCLiquidLayoutTokenizerToken *token = [[self alloc] init];
    token.tokenType = type;
    return token;
}

+ (FCLiquidLayoutTokenizerToken *)tokenWithOperatorType:(FCLiquidLayoutTokenizerOperatorType)operatorType
{
    FCLiquidLayoutTokenizerToken *token = [[self alloc] init];
    token.tokenType = FCLiquidLayoutTokenizerTokenTypeOperator;
    token.operatorType = operatorType;
    return token;
}

+ (FCLiquidLayoutTokenizerToken *)tokenWithNumber:(NSNumber *)number
{
    FCLiquidLayoutTokenizerToken *token = [[self alloc] init];
    token.tokenType = FCLiquidLayoutTokenizerTokenTypeNumber;
    token.numberValue = number;
    return token;
}

+ (FCLiquidLayoutTokenizerToken *)tokenWithSymbol:(NSString *)symbol
{
    FCLiquidLayoutTokenizerToken *token = [[self alloc] init];
    token.tokenType = FCLiquidLayoutTokenizerTokenTypeSymbol;
    token.symbolValue = symbol;
    return token;
}

@end



@interface FCLiquidLayoutTokenizer ()

@property (nonatomic, strong) NSString *inputString;

@property (nonatomic) NSInteger currentCharacterIndex;

@property (nonatomic) NSInteger statementDepth;

@end


@implementation FCLiquidLayoutTokenizer

- (instancetype)initWithString:(NSString *)string
{
    self = [super init];
    
    if (self) {
        _inputString = string;
        _currentCharacterIndex = 0;
        _statementDepth = 0;
    }
    
    return self;
}

- (unichar)peekChar
{
    if (self.currentCharacterIndex < self.inputString.length) {
        return [self.inputString characterAtIndex:self.currentCharacterIndex];
    }
    
    return 0;
}

- (unichar)nextChar
{
    if (self.currentCharacterIndex < self.inputString.length) {
        return [self.inputString characterAtIndex:self.currentCharacterIndex++];
    }
    
    return 0;
}

- (NSString *)nextSymbol
{
    NSMutableCharacterSet *allowedCharacterSet = [NSMutableCharacterSet alphanumericCharacterSet];
    NSInteger location = self.currentCharacterIndex;
    
    [allowedCharacterSet addCharactersInString:@"_"];
    
    while (YES) {
        unichar ch = [self peekChar];
        
        if (![allowedCharacterSet characterIsMember:ch]) {
            break;
        }
        
        [self nextChar];
    }
    
    NSInteger length = self.currentCharacterIndex - location;
    
    return [self.inputString substringWithRange:NSMakeRange(location, length)];
}

- (NSNumber *)nextNumber
{
    NSCharacterSet *decimalDigits = [NSCharacterSet decimalDigitCharacterSet];
    BOOL decimalDotFound = NO;
    BOOL decimalDigitFound = NO;
    BOOL exponentFound = NO;
    BOOL exponentDigitFound = NO;
    NSInteger location = self.currentCharacterIndex;
    
    while(YES) {
        unichar ch = [self peekChar];
        
        if (ch == '.') {
            if (decimalDotFound) {
                [NSException raise:@"FCLiquidLayoutTokenizerException" format:@"Invalid number, double decimal dot"];
            }
            if (exponentFound) {
                [NSException raise:@"FCLiquidLayoutTokenizerException" format:@"Invalid number, dot is not allowed in exponent part"];
            }
            
            decimalDotFound = YES;
        } else if (ch == 'e' || ch == 'E') {
            if (exponentFound) {
                [NSException raise:@"FCLiquidLayoutTokenizerException" format:@"Invalid number, double exponent sign"];
            }
            if (!decimalDigitFound) {
                [NSException raise:@"FCLiquidLayoutTokenizerException" format:@"Invalid number, no digits before exponent"];
            }
            
            exponentFound = YES;
        } else if (ch == '-' || ch == '+') {
            if (!exponentFound || exponentDigitFound) {
                break;
            }
        } else if ([decimalDigits characterIsMember:ch]) {
            if (exponentFound) {
                exponentDigitFound = YES;
            } else {
                decimalDigitFound = YES;
            }
        } else {
            break;
        }
        
        [self nextChar];
    }
    
    if (!decimalDigitFound) {
        [NSException raise:@"FCLiquidLayoutTokenizerException" format:@"Invalid number, no decimal digits"];
    }
    
    if (exponentFound && !exponentDigitFound) {
        [NSException raise:@"FCLiquidLayoutTokenizerException" format:@"Invalid number, no exponent digits"];
    }
    
    NSInteger length = self.currentCharacterIndex - location;
    NSString *numberString = [self.inputString substringWithRange:NSMakeRange(location, length)];
    
    return @([numberString doubleValue]);
}

- (void)skipWhiteSpace
{
    NSCharacterSet *spaceSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    while ([spaceSet characterIsMember:[self peekChar]]) {
        [self nextChar];
    }
}

- (FCLiquidLayoutTokenizerToken *)peekToken
{
    NSInteger currentCharacterIndex = self.currentCharacterIndex;
    FCLiquidLayoutTokenizerToken *token = [self nextToken];
    
    self.currentCharacterIndex = currentCharacterIndex;
    
    return token;
}

- (FCLiquidLayoutTokenizerToken *)nextToken
{
    NSCharacterSet *numberStartSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    NSCharacterSet *varStartSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_"];
    FCLiquidLayoutTokenizerToken *token = nil;
    
    [self skipWhiteSpace];
    
    unichar ch = [self peekChar];
    
    if (ch == '(') {
        token = [FCLiquidLayoutTokenizerToken tokenWithType:FCLiquidLayoutTokenizerTokenTypeOpenStatement];
        [self nextChar];
    } else if (ch == ')') {
        token = [FCLiquidLayoutTokenizerToken tokenWithType:FCLiquidLayoutTokenizerTokenTypeCloseStatement];
        [self nextChar];
    } else if (ch == '+') {
        token = [FCLiquidLayoutTokenizerToken tokenWithOperatorType:FCLiquidLayoutTokenizerOperatorTypePlus];
        [self nextChar];
    } else if (ch == '-') {
        token = [FCLiquidLayoutTokenizerToken tokenWithOperatorType:FCLiquidLayoutTokenizerOperatorTypeMinus];
        [self nextChar];
    } else if (ch == '*') {
        token = [FCLiquidLayoutTokenizerToken tokenWithOperatorType:FCLiquidLayoutTokenizerOperatorTypeMultiply];
        [self nextChar];
    } else if (ch == '/') {
        token = [FCLiquidLayoutTokenizerToken tokenWithOperatorType:FCLiquidLayoutTokenizerOperatorTypeDivide];
        [self nextChar];
    } else if (ch == '^') {
        token = [FCLiquidLayoutTokenizerToken tokenWithOperatorType:FCLiquidLayoutTokenizerOperatorTypePow];
        [self nextChar];
    } else if (ch == '<') {
        token = [FCLiquidLayoutTokenizerToken tokenWithOperatorType:FCLiquidLayoutTokenizerOperatorTypeMin];
        [self nextChar];
    } else if (ch == '>') {
        token = [FCLiquidLayoutTokenizerToken tokenWithOperatorType:FCLiquidLayoutTokenizerOperatorTypeMax];
        [self nextChar];
    } else if ([numberStartSet characterIsMember:ch]) {
        token = [FCLiquidLayoutTokenizerToken tokenWithNumber:[self nextNumber]];
    } else if ([varStartSet characterIsMember:ch]) {
        token = [FCLiquidLayoutTokenizerToken tokenWithSymbol:[self nextSymbol]];
    } else if (ch != 0) {
        [NSException raise:@"FCLiquidParserException" format:@"Invalid character %C", ch];
    }
    
    return token;
}

@end

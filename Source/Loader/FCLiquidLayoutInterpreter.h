//
//  FCLiquidLayoutInterpreter.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/15/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutTokenizer.h"
#import "FCLiquidLayoutEnvironment.h"
#import <Foundation/Foundation.h>

@class FCLiquidLayoutTokenizer;


@interface FCLiquidLayoutInterpreter : NSObject

+ (NSNumber *)interpretString:(NSString *)string inEnvironment:(FCLiquidLayoutEnvironment *)environment;

@end

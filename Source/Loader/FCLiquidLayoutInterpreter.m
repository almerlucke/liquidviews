//
//  FCLiquidLayoutInterpreter.m
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/15/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutInterpreter.h"
#import "FCLiquidLayoutTokenizer.h"
#import "FCLiquidLayout.h"
#import "FCLiquidLayoutInterpreterNode.h"
#import "FCLiquidLayoutInterpreterLiteralNode.h"
#import "FCLiquidLayoutInterpreterUnaryOperatorNode.h"
#import "FCLiquidLayoutInterpreterBinaryOperatorNode.h"


@interface FCLiquidLayoutInterpreter ()

@property (nonatomic, strong) FCLiquidLayoutEnvironment *environment;

@property (nonatomic, strong) FCLiquidLayoutTokenizer *tokenizer;

@end


@implementation FCLiquidLayoutInterpreter

- (instancetype)initWithString:(NSString *)string andEnvironment:(FCLiquidLayoutEnvironment *)environment
{
    self = [super init];
    if (self) {
        _tokenizer = [[FCLiquidLayoutTokenizer alloc] initWithString:string];
        _environment = environment;
    }
    
    return self;
}

- (NSMutableArray *)subStatement
{
    NSMutableArray *tokens = [NSMutableArray array];
    FCLiquidLayoutTokenizerToken *token = [self.tokenizer nextToken];
    
    while (token && token.tokenType != FCLiquidLayoutTokenizerTokenTypeCloseStatement) {
        if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeOpenStatement) {
            NSMutableArray *statement = [self subStatement];
            if (statement.count > 0) {
                [tokens addObject:statement];
            }
        } else if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeOperator) {
            [tokens addObject:token];
        } else if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeNumber) {
            [tokens addObject:token];
        } else if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeSymbol) {
            [tokens addObject:token];
        }
        
        token = [self.tokenizer nextToken];
    }
    
    if (!token) {
        [NSException raise:@"FCLiquidLayoutInterpreterException" format:@"Unmatched parenthesis"];
    }
    
    return tokens;
}

- (NSMutableArray *)mainStatement
{
    NSMutableArray *tokens = [NSMutableArray array];
    FCLiquidLayoutTokenizerToken *token = [self.tokenizer nextToken];
    
    while (token) {
        if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeOpenStatement) {
            NSMutableArray *statement = [self subStatement];
            
            if (statement.count > 0) {
                [tokens addObject:statement];
            }
        } else if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeCloseStatement) {
            [NSException raise:@"FCLiquidLayoutInterpreterException" format:@"Unmatched parenthesis"];
        } else if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeOperator) {
            [tokens addObject:token];
        } else if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeNumber) {
            [tokens addObject:token];
        } else if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeSymbol) {
            [tokens addObject:token];
        }
        
        token = [self.tokenizer nextToken];
    }
    
    return tokens;
}

- (BOOL)couldBeUnaryOperatorToken:(FCLiquidLayoutTokenizerToken *)token
{
    return (token.tokenType == FCLiquidLayoutTokenizerTokenTypeOperator &&
            (token.operatorType == FCLiquidLayoutTokenizerOperatorTypeMinus ||
             token.operatorType == FCLiquidLayoutTokenizerOperatorTypePlus));
}

- (void)simpleNodesPass:(NSMutableArray *)statement
{
    // First pass: convert numbers, symbols and statements to nodes
    for (NSInteger index = 0; index < statement.count; index++) {
        id item = statement[index];
        if ([item isKindOfClass:[NSMutableArray class]]) {
            FCLiquidLayoutInterpreterLiteralNode *statementNode = [self nodeFromStatement:item];
            statement[index] = statementNode;
        } else {
            FCLiquidLayoutTokenizerToken *token = item;
            if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeNumber) {
                statement[index] = [FCLiquidLayoutInterpreterLiteralNode literalNodeWithNumber:token.numberValue];
            } else if (token.tokenType == FCLiquidLayoutTokenizerTokenTypeSymbol) {
                NSNumber *symbolBinding = [self.environment bindingForSymbol:token.symbolValue];
                if (!symbolBinding) {
                    [NSException raise:@"FCLiquidLayoutInterpreterException" format:@"Unbound variable \"%@\"", token.symbolValue];
                }
                statement[index] = [FCLiquidLayoutInterpreterLiteralNode literalNodeWithNumber:symbolBinding];
            }
        }
    }
}

- (void)unaryOperatorNodesPass:(NSMutableArray *)statement
{
    NSMutableArray *unaryOperators = [NSMutableArray array];
    
    // Find all possible unary operators
    for (NSInteger index = 0; index < statement.count; index++) {
        id item = statement[index];
        if ([item isKindOfClass:[FCLiquidLayoutTokenizerToken class]]) {
            FCLiquidLayoutTokenizerToken *token = item;
            if ([self couldBeUnaryOperatorToken:token]) {
                id previousItem = (index > 0)? statement[index - 1] : nil;
                id nextItem = (index < (statement.count - 1))? statement[index + 1] : nil;
                
                if (!nextItem) {
                    [NSException raise:@"FCLiquidLayoutInterpreterException" format:@"Illegal operand for unary operator"];
                }
                
                if ([nextItem isKindOfClass:[FCLiquidLayoutInterpreterNode class]] && (!previousItem || [previousItem isKindOfClass:[FCLiquidLayoutTokenizerToken class]])) {
                    [unaryOperators addObject:item];
                }
            }
        }
    }
    
    // Create unary operators and replace tokens with operators, remove operands from statement
    for (FCLiquidLayoutTokenizerToken *token in unaryOperators) {
        NSInteger index = [statement indexOfObject:token];
        FCLiquidLayoutInterpreterNode *operand = statement[index + 1];
        FCLiquidLayoutInterpreterUnaryOperatorNode *unaryOperatorNode = [FCLiquidLayoutInterpreterUnaryOperatorNode operatorNodeWithType:token.operatorType];
        unaryOperatorNode.operand = operand;
        [statement replaceObjectAtIndex:index withObject:unaryOperatorNode];
        [statement removeObject:operand];
    }
}

- (void)binaryOperatorNodesPass:(NSMutableArray *)statement
{
    // Function to find binary operator with highest priority
    FCLiquidLayoutTokenizerToken *(^highestPriorityToken)(NSArray *tokens) = ^FCLiquidLayoutTokenizerToken *(NSArray *tokens) {
        FCLiquidLayoutTokenizerToken *highestPriority = nil;
        
        for (FCLiquidLayoutTokenizerToken *token in tokens) {
            if ([token isKindOfClass:[FCLiquidLayoutTokenizerToken class]]) {
                if (!highestPriority) {
                    highestPriority = token;
                } else  if (token.operatorType > highestPriority.operatorType) {
                    highestPriority = token;
                }
            }
        }
        
        return highestPriority;
    };
    
    // Find and replace all remaining operator tokens with binary operator nodes
    while (statement.count > 2) {
        FCLiquidLayoutTokenizerToken *token = highestPriorityToken(statement);
        
        if (!token) {
            break;
        }
        
        NSInteger index = [statement indexOfObject:token];
        id previousItem = (index > 0)? statement[index - 1] : nil;
        id nextItem = (index < (statement.count - 1))? statement[index + 1] : nil;
        
        if (!previousItem || !nextItem) {
            [NSException raise:@"FCLiquidLayoutInterpreterException" format:@"Missing operand for binary operator"];
        }
        
        if (![previousItem isKindOfClass:[FCLiquidLayoutInterpreterNode class]] ||
            ![nextItem isKindOfClass:[FCLiquidLayoutInterpreterNode class]]) {
            [NSException raise:@"FCLiquidLayoutInterpreterException" format:@"Illegal operand for binary operator"];
        }
        
        FCLiquidLayoutInterpreterBinaryOperatorNode *binaryOperatorNode = [FCLiquidLayoutInterpreterBinaryOperatorNode operatorNodeWithType:token.operatorType];
        binaryOperatorNode.leftOperand = previousItem;
        binaryOperatorNode.rightOperand = nextItem;
        [statement replaceObjectAtIndex:index withObject:binaryOperatorNode];
        [statement removeObject:previousItem];
        [statement removeObject:nextItem];
    }
}

- (FCLiquidLayoutInterpreterLiteralNode *)nodeFromStatement:(NSMutableArray *)statement
{
    // Reduce a statement to one node via a series of passes
    [self simpleNodesPass:statement];
    [self unaryOperatorNodesPass:statement];
    [self binaryOperatorNodesPass:statement];
    
    FCLiquidLayoutInterpreterLiteralNode *node = nil;
    
    if (statement.count > 0) {
        // return last condensed node as return value for statement
        node = statement[statement.count - 1];
    }
    
    return node;
}

- (NSNumber *)interpret
{
    NSMutableArray *statement = [self mainStatement];
    
    FCLiquidLayoutInterpreterLiteralNode *node = [self nodeFromStatement:statement];
    
    return [node resolveInEnvironment:self.environment];
}

+ (NSNumber *)interpretString:(NSString *)string inEnvironment:(FCLiquidLayoutEnvironment *)environment
{
    FCLiquidLayoutInterpreter *interpreter = [[FCLiquidLayoutInterpreter alloc] initWithString:string andEnvironment:environment];
    
    return [interpreter interpret];
}

@end

//
//  FCLiquidLayoutEnvironment.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/15/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface FCLiquidLayoutEnvironment : NSObject

- (void)popScope;

- (void)pushScopeWithBindings:(NSArray *)bindings;

- (NSNumber *)bindingForSymbol:(NSString *)symbol;

@end

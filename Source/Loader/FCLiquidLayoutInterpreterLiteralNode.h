//
//  FCLiquidLayoutParserLiteral.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 14/12/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutInterpreterNode.h"
#import <Foundation/Foundation.h>


@interface FCLiquidLayoutInterpreterLiteralNode : FCLiquidLayoutInterpreterNode

@property (nonatomic, strong) NSNumber *number;

+ (FCLiquidLayoutInterpreterLiteralNode *)literalNodeWithNumber:(NSNumber *)number;

@end

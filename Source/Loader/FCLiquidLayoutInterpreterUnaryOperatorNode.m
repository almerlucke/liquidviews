//
//  FCLiquidLayoutInterpreterUnaryOperatorNode.m
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/15/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutInterpreterUnaryOperatorNode.h"
#import "FCLiquidLayoutEnvironment.h"


@implementation FCLiquidLayoutInterpreterUnaryOperatorNode

+ (FCLiquidLayoutInterpreterUnaryOperatorNode *)operatorNodeWithType:(FCLiquidLayoutTokenizerOperatorType)type
{
    FCLiquidLayoutInterpreterUnaryOperatorNode *node = [[FCLiquidLayoutInterpreterUnaryOperatorNode alloc] init];
    node.operatorType = type;
    return node;
}

- (NSNumber *)resolveInEnvironment:(FCLiquidLayoutEnvironment *)environment
{
    NSNumber *operand = [self.operand resolveInEnvironment:environment];
    double returnValue = [operand doubleValue];
    
    if (self.operatorType == FCLiquidLayoutTokenizerOperatorTypeMinus) {
        returnValue = -returnValue;
    }
    
    return [NSNumber numberWithDouble:returnValue];
}

@end

//
//  FCLiquidLayoutParserElement.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 14/12/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import <Foundation/Foundation.h>


@class FCLiquidLayoutEnvironment;


@interface FCLiquidLayoutInterpreterNode : NSObject

- (NSNumber *)resolveInEnvironment:(FCLiquidLayoutEnvironment *)environment;

@end

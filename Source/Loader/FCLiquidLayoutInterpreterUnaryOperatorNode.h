//
//  FCLiquidLayoutInterpreterUnaryOperatorNode.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/15/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutInterpreterNode.h"
#import "FCLiquidLayoutTokenizer.h"
#import <Foundation/Foundation.h>


@interface FCLiquidLayoutInterpreterUnaryOperatorNode : FCLiquidLayoutInterpreterNode

@property (nonatomic, strong) FCLiquidLayoutInterpreterNode *operand;

@property (nonatomic) FCLiquidLayoutTokenizerOperatorType operatorType;

+ (FCLiquidLayoutInterpreterUnaryOperatorNode *)operatorNodeWithType:(FCLiquidLayoutTokenizerOperatorType)type;

@end

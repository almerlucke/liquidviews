//
//  FCLiquidLayoutParserBinaryOperator.m
//  LiquidOrientation
//
//  Created by Almer Lucke on 14/12/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutInterpreterBinaryOperatorNode.h"
#import "FCLiquidLayoutEnvironment.h"


@implementation FCLiquidLayoutInterpreterBinaryOperatorNode

+ (FCLiquidLayoutInterpreterBinaryOperatorNode *)operatorNodeWithType:(FCLiquidLayoutTokenizerOperatorType)type
{
    FCLiquidLayoutInterpreterBinaryOperatorNode *node = [[FCLiquidLayoutInterpreterBinaryOperatorNode alloc] init];
    node.operatorType = type;
    return node;
}

- (NSNumber *)resolveInEnvironment:(FCLiquidLayoutEnvironment *)environment
{
    NSNumber *leftOperand = [self.leftOperand resolveInEnvironment:environment];
    NSNumber *rightOperand = [self.rightOperand resolveInEnvironment:environment];
    double leftValue = [leftOperand doubleValue];
    double rightValue = [rightOperand doubleValue];
    double returnValue = 0;
    
    if (self.operatorType == FCLiquidLayoutTokenizerOperatorTypePlus) {
        returnValue = leftValue + rightValue;
    } else if (self.operatorType == FCLiquidLayoutTokenizerOperatorTypeMinus) {
        returnValue = leftValue - rightValue;
    } else if (self.operatorType == FCLiquidLayoutTokenizerOperatorTypeMultiply) {
        returnValue = leftValue * rightValue;
    } else if (self.operatorType == FCLiquidLayoutTokenizerOperatorTypeDivide) {
        returnValue = leftValue / rightValue;
    } else if (self.operatorType == FCLiquidLayoutTokenizerOperatorTypePow) {
        returnValue = pow(leftValue, rightValue);
    } else if (self.operatorType == FCLiquidLayoutTokenizerOperatorTypeMin) {
        returnValue = fmin(leftValue, rightValue);
    } else if (self.operatorType == FCLiquidLayoutTokenizerOperatorTypeMax) {
        returnValue = fmax(leftValue, rightValue);
    }
    
    return [NSNumber numberWithDouble:returnValue];
}

@end

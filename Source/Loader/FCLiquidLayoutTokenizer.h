//
//  FCLiquidLayoutParser.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 14/12/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, FCLiquidLayoutTokenizerTokenType)
{
    FCLiquidLayoutTokenizerTokenTypeOperator,
    FCLiquidLayoutTokenizerTokenTypeNumber,
    FCLiquidLayoutTokenizerTokenTypeSymbol,
    FCLiquidLayoutTokenizerTokenTypeOpenStatement,
    FCLiquidLayoutTokenizerTokenTypeCloseStatement
};

typedef NS_ENUM(NSInteger, FCLiquidLayoutTokenizerOperatorType)
{
    FCLiquidLayoutTokenizerOperatorTypePlus,
    FCLiquidLayoutTokenizerOperatorTypeMinus,
    FCLiquidLayoutTokenizerOperatorTypeMultiply,
    FCLiquidLayoutTokenizerOperatorTypeDivide,
    FCLiquidLayoutTokenizerOperatorTypePow,
    FCLiquidLayoutTokenizerOperatorTypeMin,
    FCLiquidLayoutTokenizerOperatorTypeMax
};


@interface FCLiquidLayoutTokenizerToken : NSObject

@property (nonatomic, readonly) FCLiquidLayoutTokenizerTokenType tokenType;

@property (nonatomic, readonly) FCLiquidLayoutTokenizerOperatorType operatorType;

@property (nonatomic, strong, readonly) NSNumber *numberValue;

@property (nonatomic, copy, readonly) NSString *symbolValue;

@end


@interface FCLiquidLayoutTokenizer : NSObject

- (instancetype)initWithString:(NSString *)string;

- (FCLiquidLayoutTokenizerToken *)nextToken;

@end

//
//  FCLiquidLayoutLoader.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 15/12/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayout.h"
#import <UIKit/UIKit.h>


@interface FCLiquidLayoutLoader : NSObject

+ (void)loadLayout:(NSString *)layoutPath forObject:(id<FCLiquidLayout>)root;

+ (void)loadLayoutForObject:(id<FCLiquidLayout>)root;

@end

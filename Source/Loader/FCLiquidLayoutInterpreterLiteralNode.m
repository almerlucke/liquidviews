//
//  FCLiquidLayoutParserLiteral.m
//  LiquidOrientation
//
//  Created by Almer Lucke on 14/12/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutInterpreterLiteralNode.h"


@implementation FCLiquidLayoutInterpreterLiteralNode

+ (FCLiquidLayoutInterpreterLiteralNode *)literalNodeWithNumber:(NSNumber *)number
{
    FCLiquidLayoutInterpreterLiteralNode *node = [[FCLiquidLayoutInterpreterLiteralNode alloc] init];
    node.number = number;
    return node;
}

- (NSNumber *)resolveInEnvironment:(FCLiquidLayoutEnvironment *)environment
{
    return self.number;
}

@end

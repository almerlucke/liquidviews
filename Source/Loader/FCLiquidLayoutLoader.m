//
//  FCLiquidLayoutLoader.m
//  LiquidOrientation
//
//  Created by Almer Lucke on 15/12/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutLoader.h"
#import "FCLiquidLayoutEnvironment.h"
#import "FCLiquidLayoutInterpreter.h"
#import "UIDevice+DeviceName.h"


@implementation FCLiquidLayoutLoader

+ (void)loadLayout:(NSString *)layoutPath forObject:(id<FCLiquidLayout>)root
{
    NSData *rootLayoutData = [NSData dataWithContentsOfFile:layoutPath];
    
    if (rootLayoutData) {
        NSDictionary *rootLayout = [NSJSONSerialization JSONObjectWithData:rootLayoutData
                                                                   options:0
                                                                     error:nil];
        if (rootLayout) {
            FCLiquidLayoutEnvironment *environment = [[FCLiquidLayoutEnvironment alloc] init];
    
            [self Layout:rootLayout object:root environment:environment];
        }
    }
}

+ (NSString *)deviceSpecificLiquidLayoutID:(NSString *)liquidLayoutID
{
    return [NSString stringWithFormat:@"%@_%@", liquidLayoutID, [UIDevice strippedDeviceName]];
}

+ (NSString *)platformSpecificLiquidLayoutID:(NSString *)liquidLayoutID
{
    return [NSString stringWithFormat:@"%@_%@", liquidLayoutID, [UIDevice minimizedDeviceName]];
}

+ (void)loadLayoutForObject:(id<FCLiquidLayout>)root
{
    NSString *deviceLiquidLayoutID = [self deviceSpecificLiquidLayoutID:root.liquidLayoutID];
    NSString *platformLiquidLayoutID = [self platformSpecificLiquidLayoutID:root.liquidLayoutID];
    NSString *defaultPath = [[NSBundle mainBundle] pathForResource:root.liquidLayoutID ofType:@"json"];
    NSString *deviceSpecificPath = [[NSBundle mainBundle] pathForResource:deviceLiquidLayoutID ofType:@"json"];
    NSString *platformSpecificPath = [[NSBundle mainBundle] pathForResource:platformLiquidLayoutID ofType:@"json"];
    
    if (deviceSpecificPath) {
        [self loadLayout:deviceSpecificPath forObject:root];
    } else if (platformSpecificPath) {
        [self loadLayout:platformSpecificPath forObject:root];
    } else if (defaultPath) {
        [self loadLayout:defaultPath forObject:root];
    }
}

+ (id)layoutDictionary:(NSDictionary *)layoutDictionary key:(NSString *)key
{
    // Give the option to add device specific margin, padding, frame etc.. in layout json file
    NSString *deviceSpecificKey = [NSString stringWithFormat:@"%@_%@", key, [UIDevice strippedDeviceName]];
    NSString *platformSpecificKey = [NSString stringWithFormat:@"%@_%@", key, [UIDevice minimizedDeviceName]];
    
    id value = layoutDictionary[deviceSpecificKey];
    
    if (!value) {
        value = layoutDictionary[platformSpecificKey];
        
        if (!value) {
            value = layoutDictionary[key];
        }
    }
    
    return value;
}

+ (void)Layout:(NSDictionary *)layout
        object:(id<FCLiquidLayout>)object
    environment:(FCLiquidLayoutEnvironment *)environment
{
    NSArray *vars = layout[@"vars"];
    
    // First push variable scope from layout dictionary
    if ([vars isKindOfClass:[NSArray class]]) {
        [environment pushScopeWithBindings:vars];
    }
    
    object.supportsLiquidLayout = YES;
    
    id layoutElement;
    
    if ((layoutElement = [self layoutDictionary:layout key:@"margin"])) {
        object.liquidMargin = [self marginFromObject:layoutElement environment:environment];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"portraitMargin"])) {
        object.liquidPortraitMargin = [self marginFromObject:layoutElement environment:environment];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"landscapeMargin"])) {
        object.liquidLandscapeMargin = [self marginFromObject:layoutElement environment:environment];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"marginUnits"])) {
        object.liquidMarginUnits = [self unitsFromObject:layoutElement];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"portraitMarginUnits"])) {
        object.liquidPortraitMarginUnits = [self unitsFromObject:layoutElement];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"landscapeMarginUnits"])) {
        object.liquidLandscapeMarginUnits = [self unitsFromObject:layoutElement];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"padding"])) {
        object.liquidPadding = [self paddingFromObject:layoutElement environment:environment];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"portraitPadding"])) {
         object.liquidPortraitPadding = [self paddingFromObject:layoutElement environment:environment];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"landscapePadding"])) {
        object.liquidLandscapePadding = [self paddingFromObject:layoutElement environment:environment];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"paddingUnits"])) {
        object.liquidPaddingUnits = [self unitsFromObject:layoutElement];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"portraitPaddingUnits"])) {
        object.liquidPortraitPaddingUnits = [self unitsFromObject:layoutElement];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"landscapePaddingUnits"])) {
        object.liquidLandscapePaddingUnits = [self unitsFromObject:layoutElement];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"frame"])) {
        object.liquidFrame = [self frameFromObject:layoutElement environment:environment];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"portraitFrame"])) {
        object.liquidPortraitFrame = [self frameFromObject:layoutElement environment:environment];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"landscapeFrame"])) {
        object.liquidLandscapeFrame = [self frameFromObject:layoutElement environment:environment];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"frameUnits"])) {
        object.liquidFrameUnits = [self unitsFromObject:layoutElement];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"portraitFrameUnits"])) {
        object.liquidPortraitFrameUnits = [self unitsFromObject:layoutElement];
    }
    
    if ((layoutElement = [self layoutDictionary:layout key:@"landscapeFrameUnits"])) {
        object.liquidLandscapeFrameUnits = [self unitsFromObject:layoutElement];
    }
    
    NSArray *children = layout[@"children"];
    
    if ([children isKindOfClass:[NSArray class]]) {
        for (NSDictionary *childDictionary in children) {
            if ([childDictionary isKindOfClass:[NSDictionary class]]) {
                NSString *liquidLayoutID = childDictionary[@"liquidID"];
                if ([liquidLayoutID isKindOfClass:[NSString class]]) {
                    id<FCLiquidLayout> childObject = [object childWithLiquidLayoutID:liquidLayoutID];
                    
                    if (childObject) {
                        [self Layout:childDictionary object:childObject environment:environment];
                    }
                }
            }
        }
    }
    
    // End layout scope
    if ([vars isKindOfClass:[NSDictionary class]]) {
        [environment popScope];
    }
}

+ (id)object:(id)obj1 or:(id)obj2
{
    return (obj1)? obj1 : obj2;
}

+ (CGFloat)valueFromObject:(id)object
               environment:(FCLiquidLayoutEnvironment *)environment
{
    CGFloat value = 0;
    
    if ([object isKindOfClass:[NSNumber class]]) {
        value = [object doubleValue];
    } else if ([object isKindOfClass:[NSString class]]) {
        @try {
            NSNumber *number = [FCLiquidLayoutInterpreter interpretString:object inEnvironment:environment];
            value = [number doubleValue];
        }
        @catch (NSException *exception) {
            NSLog(@"FCLiquidLayoutLoaderException: %@", exception);
        }
    }
    
    return value;
}

+ (FCLiquidUnit)unitFromObject:(id)object
{
    FCLiquidUnit unit = FCLiquidUnitPercentage;
    
    if ([object isKindOfClass:[NSString class]]) {
        NSString *unitString = object;
        if ([unitString isEqualToString:@"pts"]) {
            unit = FCLiquidUnitPoints;
        }
    }
    
    return unit;
}

+ (FCLiquidUnits)unitsFromObject:(id)object
{
    FCLiquidUnits units = {FCLiquidUnitPercentage, FCLiquidUnitPercentage, FCLiquidUnitPercentage, FCLiquidUnitPercentage};
    
    if ([object isKindOfClass:[NSString class]]) {
        units.unit1 = units.unit2 = units.unit3 = units.unit4 = [self unitFromObject:object];
    } else if ([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *unitDictionary = object;
        NSString *unit1 = [self object:unitDictionary[@"top"] or:unitDictionary[@"x"]];
        NSString *unit2 = [self object:unitDictionary[@"left"] or:unitDictionary[@"y"]];
        NSString *unit3 = [self object:unitDictionary[@"bottom"] or:unitDictionary[@"w"]];
        NSString *unit4 = [self object:unitDictionary[@"right"] or:unitDictionary[@"h"]];
        
        units.unit1 = [self unitFromObject:unit1];
        units.unit2 = [self unitFromObject:unit2];
        units.unit3 = [self unitFromObject:unit3];
        units.unit4 = [self unitFromObject:unit4];
    }
    
    return units;
}

+ (FCLiquidMargin)marginFromObject:(id)object
                       environment:(FCLiquidLayoutEnvironment *)environment
{
    FCLiquidMargin margin = {0, 0, 0, 0};
    
    if ([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *marginDictionary = object;
        margin.top = [self valueFromObject:marginDictionary[@"top"] environment:environment];
        margin.left = [self valueFromObject:marginDictionary[@"left"] environment:environment];
        margin.bottom = [self valueFromObject:marginDictionary[@"bottom"] environment:environment];
        margin.right = [self valueFromObject:marginDictionary[@"right"] environment:environment];
    } else {
        CGFloat value = [self valueFromObject:object environment:environment];
        margin.top = margin.left = margin.bottom = margin.right = value;
    }
    
    return margin;
}

+ (FCLiquidPadding)paddingFromObject:(id)object
                         environment:(FCLiquidLayoutEnvironment *)environment
{
    FCLiquidPadding padding = {0, 0, 0, 0};
    
    if ([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *paddingDictionary = object;
        padding.top = [self valueFromObject:paddingDictionary[@"top"] environment:environment];
        padding.left = [self valueFromObject:paddingDictionary[@"left"] environment:environment];
        padding.bottom = [self valueFromObject:paddingDictionary[@"bottom"] environment:environment];
        padding.right = [self valueFromObject:paddingDictionary[@"right"] environment:environment];
    } else {
        CGFloat value = [self valueFromObject:object environment:environment];
        padding.top = padding.left = padding.bottom = padding.right = value;
    }
    
    return padding;
}

+ (FCLiquidFrame)frameFromObject:(id)object
                     environment:(FCLiquidLayoutEnvironment *)environment
{
    FCLiquidFrame frame = {0, 0, 0, 0};
    
    if ([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *frameDictionary = object;
        frame.origin.x = [self valueFromObject:frameDictionary[@"x"] environment:environment];
        frame.origin.y = [self valueFromObject:frameDictionary[@"y"] environment:environment];
        frame.size.width = [self valueFromObject:frameDictionary[@"w"] environment:environment];
        frame.size.height = [self valueFromObject:frameDictionary[@"h"] environment:environment];
    } else {
        CGFloat value = [self valueFromObject:object environment:environment];
        frame.origin.x = frame.origin.y = frame.size.width = frame.size.height = value;
    }
    
    return frame;
}

@end

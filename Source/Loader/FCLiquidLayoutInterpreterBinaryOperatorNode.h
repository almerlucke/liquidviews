//
//  FCLiquidLayoutParserBinaryOperator.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 14/12/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "FCLiquidLayoutInterpreterNode.h"
#import "FCLiquidLayoutTokenizer.h"
#import <Foundation/Foundation.h>


@interface FCLiquidLayoutInterpreterBinaryOperatorNode : FCLiquidLayoutInterpreterNode

@property (nonatomic, strong) FCLiquidLayoutInterpreterNode *leftOperand;

@property (nonatomic, strong) FCLiquidLayoutInterpreterNode *rightOperand;

@property (nonatomic) FCLiquidLayoutTokenizerOperatorType operatorType;

+ (FCLiquidLayoutInterpreterBinaryOperatorNode *)operatorNodeWithType:(FCLiquidLayoutTokenizerOperatorType)type;

@end

//
//  NSObject+MethodSwizzling.m
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/7/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import <objc/runtime.h>
#import "NSObject+RuntimeHelpers.h"


@implementation NSObject (RuntimeHelpers)

+ (void)swizzle:(SEL)originalSelector for:(SEL)swizzledSelector
{
    Class class = [self class];
    
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    
    BOOL didAddMethod = class_addMethod(class,
                                        originalSelector,
                                        method_getImplementation(swizzledMethod),
                                        method_getTypeEncoding(swizzledMethod));
    
    if (didAddMethod) {
        class_replaceMethod(class,
                            swizzledSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}

- (void)setAssoc:(id)obj forKey:(const void *)key
{
    objc_setAssociatedObject(self, key, obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setAssoc:(id)obj forKey:(const void *)key policy:(objc_AssociationPolicy)policy
{
    objc_setAssociatedObject(self, key, obj, policy);
}

- (id)assocForKey:(const void *)key
{
    return objc_getAssociatedObject(self, key);
}

@end

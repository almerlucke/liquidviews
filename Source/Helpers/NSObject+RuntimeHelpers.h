//
//  NSObject+MethodSwizzling.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/7/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <objc/runtime.h>


@interface NSObject (RuntimeHelpers)

+ (void)swizzle:(SEL)originalSelector for:(SEL)swizzledSelector;

- (void)setAssoc:(id)obj forKey:(const void *)key;

- (void)setAssoc:(id)obj forKey:(const void *)key policy:(objc_AssociationPolicy)policy;

- (id)assocForKey:(const void *)key;

@end

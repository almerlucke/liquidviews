//
//  UIDevice+DeviceName.m
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/16/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import "UIDevice+DeviceName.h"
#import <sys/utsname.h>


@implementation UIDevice (DeviceName)

+ (NSString *)deviceName
{
    static NSString *deviceName = nil;
    struct utsname systemInfo;
    NSString* code = nil;
    
    if (deviceName) {
        return deviceName;
    }
    
    uname(&systemInfo);
    
    code = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    NSString *devicesPath = [[NSBundle mainBundle] pathForResource:@"devices" ofType:@"plist"];
    NSDictionary *deviceNamesByCode = [NSDictionary dictionaryWithContentsOfFile:devicesPath];
    
    deviceName = deviceNamesByCode[code][@"name"];
    
    if (!deviceName) {
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        } else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        } else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        } else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
}

+ (NSString *)strippedDeviceName
{
    return [[[self deviceName] stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString];
}

+ (NSString *)minimizedDeviceName
{
    NSString *deviceName = [self deviceName];
    NSArray *parts = [deviceName componentsSeparatedByString:@" "];
    
    if (parts.count > 0) {
        return [parts[0] lowercaseString];
    }
    
    return nil;
}

@end

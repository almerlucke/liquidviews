//
//  UIDevice+DeviceName.h
//  LiquidOrientation
//
//  Created by Almer Lucke on 12/16/15.
//  Copyright © 2015 farcoding. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIDevice (DeviceName)

+ (NSString *)deviceName;
+ (NSString *)strippedDeviceName;
+ (NSString *)minimizedDeviceName;

@end

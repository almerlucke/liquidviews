//
//  ViewController.m
//  LiquidOrientation
//
//  Created by aFrogleap on 11/8/12.
//  Copyright (c) 2012 farcoding. All rights reserved.
//


#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+FCLiquidLayout.h"
#import "FCLiquidLayoutLoader.h"


@interface ViewController ()
@end

@implementation ViewController

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [CATransaction begin];
    for (UIView *subview in self.view.subviews) {
        CAKeyframeAnimation *scaleAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
        NSArray *scaleValues = @[@1.0, @0.1, @1.0];
        
        scaleAnimation.values = scaleValues;
        scaleAnimation.duration = coordinator.transitionDuration;
        scaleAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        scaleAnimation.fillMode = kCAFillModeForwards;
        scaleAnimation.removedOnCompletion = YES;
        
        CAKeyframeAnimation *rotateAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation"];
        NSArray *rotationValues = @[@0.0, @6.279];
        
        rotateAnimation.values = rotationValues;
        rotateAnimation.duration = coordinator.transitionDuration;
        rotateAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        rotateAnimation.fillMode = kCAFillModeForwards;
        rotateAnimation.removedOnCompletion = YES;
        
        CAAnimationGroup *group = [CAAnimationGroup animation];
        group.animations = @[rotateAnimation, scaleAnimation];
        group.duration = coordinator.transitionDuration;
        group.removedOnCompletion = YES;
        
        [subview.layer addAnimation:group forKey:@"orientation"];
    }
    [CATransaction commit];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *profilePictureView = [[UIImageView alloc] init];
    profilePictureView.liquidLayoutID = @"profilePicture";
    profilePictureView.contentMode = UIViewContentModeScaleAspectFill;
    profilePictureView.image = [UIImage imageNamed:@"profilePicture"];
    profilePictureView.clipsToBounds = YES;
    profilePictureView.backgroundColor = [UIColor brownColor];
    profilePictureView.layer.cornerRadius = 4.0;
    [self.view addSubview:profilePictureView];
    
    UIView *profileTitleContainer = [[UIView alloc] init];
    profileTitleContainer.liquidLayoutID = @"profileTitleContainer";
    profileTitleContainer.backgroundColor = [UIColor colorWithRed:1 green:0.9 blue:0.9 alpha:1.0];
    profileTitleContainer.layer.cornerRadius = 4.0;
    [self.view addSubview:profileTitleContainer];
    
    UILabel *profileTitleLabel = [[UILabel alloc] init];
    profileTitleLabel.liquidLayoutID = @"profileTitleLabel";
    profileTitleLabel.clipsToBounds = YES;
    profileTitleLabel.text = @"'Dit is een titel, dat is echt toppertje!'";
    profileTitleLabel.numberOfLines = 0;
    profileTitleLabel.textColor = [UIColor colorWithRed:0.6 green:0.2 blue:0.2 alpha:1];
    profileTitleLabel.font = [UIFont boldSystemFontOfSize:24.0];
    profileTitleLabel.textAlignment = NSTextAlignmentCenter;
    profileTitleLabel.layer.cornerRadius = 4.0;
    [profileTitleContainer addSubview:profileTitleLabel];
    
    UITextView *infoTextView = [[UITextView alloc] init];
    infoTextView.liquidLayoutID = @"infoTextView";
    infoTextView.textColor = [UIColor darkGrayColor];
    infoTextView.font = [UIFont systemFontOfSize:18.0];
    infoTextView.editable = NO;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;
    
    NSString *str = @"The revelations underscore the fact that in the post-Snowden environment, privacy is less of a given and more of a fast-paced cat and mouse game. An encryption network, developed by the military, gains popularity among a public increasingly worried about government surveillance. The network is then hacked by the government that created it. Of course, you don’t have to be the NSA to crack TO; you just need a bit of money. Two researchers, Alexander Volynkin and Michael McCord, will presenting at the popular Black Hat conference next month, a provocative session called “You Don’t Have To Be the NSA to Break TOR: Deanonymzing Users On a Budget.” They report that they can crack TOR and disclose a specific user’s identity for just $3000.";
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:str];
    
    [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attrString.length)];
    [attrString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18.0] range:NSMakeRange(0, attrString.length)];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, attrString.length)];
    infoTextView.attributedText = attrString;
    
    [self.view addSubview:infoTextView];
    
    UIButton *pinterestButton = [UIButton buttonWithType:UIButtonTypeCustom];
    pinterestButton.liquidLayoutID = @"pinterestButton";
    [pinterestButton setImage:[UIImage imageNamed:@"pinterest"] forState:UIControlStateNormal];
    [self.view addSubview:pinterestButton];
    
    UIButton *facebookButton = [UIButton buttonWithType:UIButtonTypeCustom];
    facebookButton.liquidLayoutID = @"facebookButton";
    [facebookButton setImage:[UIImage imageNamed:@"facebook"] forState:UIControlStateNormal];
    [self.view addSubview:facebookButton];
    
    UIButton *twitterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    twitterButton.liquidLayoutID = @"twitterButton";
    [twitterButton setImage:[UIImage imageNamed:@"twitter"] forState:UIControlStateNormal];
    [self.view addSubview:twitterButton];
    
    UIButton *googleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    googleButton.liquidLayoutID = @"googleButton";
    [googleButton setImage:[UIImage imageNamed:@"google"] forState:UIControlStateNormal];
    [self.view addSubview:googleButton];
    
    [self.view loadLiquidLayout];
}

- (void)showLiquidLayerViewController
{
    NSLog(@"hello!");
}

@end
